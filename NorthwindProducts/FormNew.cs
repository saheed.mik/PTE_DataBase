﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NorthwindProducts
{
    public partial class FormNew : Form
    {
        private NorthwindEntities entities;
        OpenFileDialog openFileDialog;
        Category category;

        public FormNew()
        {
            InitializeComponent();
        }

        public FormNew(NorthwindEntities entities):this()
        {
            this.entities = entities;
            openFileDialog = new OpenFileDialog();
            category = new Category();
        }

        private void btnLoadPicture_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    txtBxPIxLocation.Text = openFileDialog.FileName;
                    
                    byte[] imageBytes;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        openFileDialog.OpenFile().CopyTo(ms);
                        imageBytes = ms.ToArray();
                    }

                    category.Picture = imageBytes;
                    pictureBox.Image = (Image)new ImageConverter().ConvertFrom(imageBytes);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("There is an error while trying to open selected Image \n" + ex.Message);
                }
            }

        }

        private void btnNewProduct_Click(object sender, EventArgs e)
        {
            try
            {
                category.CategoryName = txtBoxName.Text;
                category.Description = txtBoxDesc.Text;

                entities.Categories.Add(category);
                entities.SaveChanges();
            }
            catch (FormatException ex)
            {
                MessageBox.Show("THere is a Format Error : \n" + ex.Message,"ERROR");
            }
            catch (Exception ex)
            {
                MessageBox.Show("THere is an error saving the category : \n" + ex.Message, "ERROR");
            }

        }
    }
}
