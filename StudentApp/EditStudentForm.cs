﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace StudentApp
{
    public partial class EditStudentForm : StudentApp.NewStudentForm
    {
        private readonly StudentDbModelContainer studentContainer;

        public EditStudentForm(StudentDbModelContainer studentContainer)
        {
            InitializeComponent();
            this.studentContainer = studentContainer;
            foreach (Control item in base.Controls)
            {
                if (item is Button)
                {
                    item.Text = "Save Student";

                }
            }
        }

        
    }
}
