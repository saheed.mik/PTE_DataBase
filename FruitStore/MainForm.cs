﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FruitStore
{
    public partial class MainForm : Form
    {
        ProductModel dataModel;
        public MainForm()
        {
            InitializeComponent();

            dataModel = new ProductModel();
            if (!dataModel.Products.Any())
            {
                modifyDataToolStripMenuItem.Enabled = false;
                purchaseToolStripMenuItem.Enabled = false;

            }

        }

        private void createDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            try
            {

                if (dataModel.Products.Any())
                {
                    var result = MessageBox.Show("The Database has already been created." +
                        "\n\n    Would you like to RESET it?","", MessageBoxButtons.YesNo);
                    if (result == DialogResult.No)
                    {
                        return;
                    }

                    dataModel.Products.RemoveRange(dataModel.Products);
                    dataModel.SaveChanges();
                }

                //read from csv

                IList<Product> all_csv_products = new List<Product>();

                using (var reader = new StreamReader(@".\Practice10_fruits.txt"))
                {
                   
                    while (!reader.EndOfStream)
                    {
                        var product = new Product();

                        var line = reader.ReadLine();
                        var values = line.Split(';');

                        product.ProductName = values[0];
                        product.UnitPrice = Double.Parse(values[1]);
                        product.StockValue = Double.Parse(values[2]);
                        product.Picture = values[3];

                        all_csv_products.Add(product);
                    }
                }

                dataModel.Products.AddRange(all_csv_products);
                dataModel.SaveChanges();

                modifyDataToolStripMenuItem.Enabled = true;
                purchaseToolStripMenuItem.Enabled = true;
            }
            catch (IOException ex)
            {
                MessageBox.Show("There has been an error reading data from the csv file : " + ex.Message, "ERROR");
            }
            catch (Exception)
            {
                MessageBox.Show("An error have occured", "ERROR");
            }
        }

        private void purchaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PurchaseForm purchaseForm = new PurchaseForm(dataModel);
            purchaseForm.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void modifyDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModifyForm modifyForm = new ModifyForm(dataModel);
            modifyForm.ShowDialog();
        }
    }
}
