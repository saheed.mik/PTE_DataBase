﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentApp
{
    public partial class DeleteStudentForm : Form
    {
        private StudentDbModelContainer studentContainer;

        public DeleteStudentForm()
        {
            InitializeComponent();
        }

        public DeleteStudentForm(StudentDbModelContainer studentContainer):this()
        {
            this.studentContainer = studentContainer;
            
            FillData();
        }

        private void FillData()
        {
            listBoxStudents.Items.Clear();

            foreach (var student in studentContainer.Students)
            {
                listBoxStudents.Items.Add(student);
            }
        }

        private void btnDeleteSelect_Click(object sender, EventArgs e)
        {
            if (listBoxStudents.Items.Count == 0)
            {
                MessageBox.Show("Student Database is currently empty");
            }
            else if (listBoxStudents.SelectedIndices.Count == 0)
            {
                MessageBox.Show("You have not selected any student.");
            }
            else
            {
                IList<Student> studentToDelete = new List<Student>();

                //collected students to be deleted
                foreach (int index in listBoxStudents.SelectedIndices)
                {
                    int studentID = ((Student)listBoxStudents.Items[index]).Id;
                    Student student = studentContainer.Students.Find(studentID);
                    if (student != null) 
                    {
                        studentToDelete.Add(student);
                    }
                }

                studentContainer.Students.RemoveRange(studentToDelete);

                try
                {
                    int affected = studentContainer.SaveChanges();
                    MessageBox.Show(affected + "  Studentds has been deleted.", "Informaton");
                    FillData();
                    //listBoxStudents.Refresh();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error occored while trying to delete : " + ex.Message, "ERROR");
                    throw;
                }
            }
        }
    }
}
