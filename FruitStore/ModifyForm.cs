﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FruitStore
{
    public partial class ModifyForm : Form
    {
        private ProductModel dataModel;
        private Product selectedProduct;

        public ModifyForm()
        {
            InitializeComponent();
        }

        public ModifyForm(ProductModel dataModel):this()
        {
            this.dataModel = dataModel;
            listBox.SelectedIndexChanged += ListBox_SelectedIndexChanged;
            listBox.Columns.Add("Name", -2, HorizontalAlignment.Left);
            listBox.Columns.Add("Stock Value", -2, HorizontalAlignment.Left);
            listBox.FullRowSelect = true;
            listBox.GridLines = true;
            listBox.MultiSelect = false;



            btnCancel.Click += BtnCancel_Click;
            btnDelete.Click += BtnDelete_Click;
            btnModify.Click += BtnModify_Click;
            btnNew.Click += BtnNew_Click;

            populateListBox();
            
        }

        private void populateListBox()
        {
            foreach (var product in dataModel.Products)
            {
                //listBox.Items.Add(item);
                ListViewItem item = new ListViewItem(product.ProductName, 0);
                item.SubItems.Add(product.StockValue.ToString());
                item.SubItems.Add(product.ProductID.ToString());

                listBox.Items.Add(item);
            }

        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            NewForm newProductForm = new NewForm(dataModel);
            newProductForm.ShowDialog();
            populateListBox();
        }

        private void BtnModify_Click(object sender, EventArgs e)
        {
            try
            {
                var product = dataModel.Products.Find(selectedProduct.ProductID);
                if (product != null)
                {
                    product.StockValue = Double.Parse(txtBxStockValue.Text);
                    product.UnitPrice = Double.Parse(txtBxUnitPrice.Text);

                    dataModel.SaveChanges();
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("There is a format input error !");
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            var product = dataModel.Products.Find(selectedProduct.ProductID);
            if (product!=null)
            {
                dataModel.Products.Remove(product);
                dataModel.SaveChanges();
            }
            populateListBox();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listBox.SelectedItems.Count >0)
            {
                try
                {

                    ListViewItem item = this.listBox.SelectedItems[0];
                    selectedProduct = dataModel.Products.Find(int.Parse(item.SubItems[2].Text)) ?? new Product();

                    //selectedProduct = (Product)listBox.CheckedItems;
                    txtBxID.Text = selectedProduct.ProductID.ToString();
                    txtBxName.Text = selectedProduct.ProductName;
                    txtBxStockValue.Text = selectedProduct.StockValue.ToString();// + " Kg"
                    txtBxUnitPrice.Text = selectedProduct.UnitPrice.ToString(); //+ " Ft"
                    pictureBox.Image = Image.FromFile(@"images\" + selectedProduct.Picture);
                }
                catch (IOException)
                {
                    MessageBox.Show("Error enccountered while reading image of this product.");
                    //throw;
                } 
            }
        }

        
    }
}
