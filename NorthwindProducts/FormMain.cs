﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NorthwindProducts
{
    public partial class FormMain : Form
    {
        NorthwindEntities entities;
        public FormMain()
        {
            InitializeComponent();
            entities = new NorthwindEntities();
        }

        private void btnLoadData_Click(object sender, EventArgs e)
        {
            lstBxProducts.Items.Clear();
            foreach (var item in entities.Categories)
            {
                lstBxProducts.Items.Add(item);
            }
        }

        private void lstBxProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            showDetalsFor((Category)lstBxProducts.SelectedItem);
        }

        private void showDetalsFor(Category selectedItem)
        {
            txtBoxCode.Text =  selectedItem.CategoryID.ToString();
            txtBoxDesc.Text = selectedItem.Description;
            txtBoxName.Text = selectedItem.CategoryName;
            //(Bitmap)((new ImageConverter()).ConvertFrom(selectedItem.Picture));
            pictureBox.Image = (Image)new ImageConverter().ConvertFrom(selectedItem.Picture);

        }

        private void btnNewProduct_Click(object sender, EventArgs e)
        {
            FormNew formNew = new FormNew(entities);
            formNew.ShowDialog(this);
            btnLoadData.PerformClick();
        }
    }
}
