﻿namespace StudentApp
{
    partial class NewStudentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddStudent = new System.Windows.Forms.Button();
            this.txtBxID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBxGrade = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBxFamilyName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBxGivenNames = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnAddStudent
            // 
            this.btnAddStudent.Location = new System.Drawing.Point(188, 312);
            this.btnAddStudent.Name = "btnAddStudent";
            this.btnAddStudent.Size = new System.Drawing.Size(115, 38);
            this.btnAddStudent.TabIndex = 0;
            this.btnAddStudent.Text = "Add Student";
            this.btnAddStudent.UseVisualStyleBackColor = true;
            this.btnAddStudent.Click += new System.EventHandler(this.btnAddStudent_Click);
            // 
            // txtBxID
            // 
            this.txtBxID.Location = new System.Drawing.Point(188, 68);
            this.txtBxID.Name = "txtBxID";
            this.txtBxID.Size = new System.Drawing.Size(204, 20);
            this.txtBxID.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(95, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "CODE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(92, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Average Grade";
            // 
            // txtBxGrade
            // 
            this.txtBxGrade.Location = new System.Drawing.Point(185, 215);
            this.txtBxGrade.Name = "txtBxGrade";
            this.txtBxGrade.Size = new System.Drawing.Size(204, 20);
            this.txtBxGrade.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(95, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Family Name";
            // 
            // txtBxFamilyName
            // 
            this.txtBxFamilyName.Location = new System.Drawing.Point(188, 111);
            this.txtBxFamilyName.Name = "txtBxFamilyName";
            this.txtBxFamilyName.Size = new System.Drawing.Size(204, 20);
            this.txtBxFamilyName.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(95, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Given Names";
            // 
            // txtBxGivenNames
            // 
            this.txtBxGivenNames.Location = new System.Drawing.Point(188, 162);
            this.txtBxGivenNames.Name = "txtBxGivenNames";
            this.txtBxGivenNames.Size = new System.Drawing.Size(204, 20);
            this.txtBxGivenNames.TabIndex = 7;
            // 
            // NewStudentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 450);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBxGivenNames);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBxFamilyName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBxGrade);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBxID);
            this.Controls.Add(this.btnAddStudent);
            this.Name = "NewStudentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add New Student Record";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddStudent;
        private System.Windows.Forms.TextBox txtBxID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBxGrade;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBxFamilyName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBxGivenNames;
    }
}