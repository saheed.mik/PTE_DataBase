﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentApp
{
    public partial class NewStudentForm : Form
    {
        private StudentDbModelContainer studentContainer;

        public NewStudentForm()
        {
            InitializeComponent();
        }

        public NewStudentForm(StudentDbModelContainer studentContainer):this()
        {
            this.studentContainer = studentContainer;
        }

        private void btnAddStudent_Click(object sender, EventArgs e)
        {
            try
            {
                Student student = new Student();

                student.AverageGrade = double.Parse(txtBxGrade.Text);
                student.FamilyName = txtBxFamilyName.Text;
                student.GivenNames = txtBxGivenNames.Text;

                studentContainer.Students.Add(student);
                studentContainer.SaveChanges();

                MessageBox.Show("The student : " + txtBxID.Text + " has been saved successfully.");

                txtBxFamilyName.Clear();
                txtBxGivenNames.Clear();
                txtBxID.Clear();
                txtBxGrade.Clear();
            }
            catch (FormatException ex)
            {
                MessageBox.Show("There is a Format Error : " + ex.Message, "Error");
                txtBxID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("An Error has ocurred : " + ex.Message, "Error");
            }


        }
    }
}
