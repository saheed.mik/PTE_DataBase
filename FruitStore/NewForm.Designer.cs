﻿namespace FruitStore
{
    partial class NewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSaveProduct = new System.Windows.Forms.Button();
            this.btnGetPicture = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBxStockPrice = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBxUnitPrice = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBxName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSaveProduct
            // 
            this.btnSaveProduct.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSaveProduct.Location = new System.Drawing.Point(151, 351);
            this.btnSaveProduct.Name = "btnSaveProduct";
            this.btnSaveProduct.Size = new System.Drawing.Size(143, 23);
            this.btnSaveProduct.TabIndex = 26;
            this.btnSaveProduct.Text = "Save Product";
            this.btnSaveProduct.UseVisualStyleBackColor = true;
            this.btnSaveProduct.Click += new System.EventHandler(this.btnSaveProduct_Click);
            // 
            // btnGetPicture
            // 
            this.btnGetPicture.Location = new System.Drawing.Point(101, 214);
            this.btnGetPicture.Name = "btnGetPicture";
            this.btnGetPicture.Size = new System.Drawing.Size(75, 23);
            this.btnGetPicture.TabIndex = 25;
            this.btnGetPicture.Text = "Get Picture";
            this.btnGetPicture.UseVisualStyleBackColor = true;
            this.btnGetPicture.Click += new System.EventHandler(this.btnGetPicture_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(101, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 36;
            this.label5.Text = "Picture:";
            // 
            // txtBxStockPrice
            // 
            this.txtBxStockPrice.Location = new System.Drawing.Point(222, 120);
            this.txtBxStockPrice.Name = "txtBxStockPrice";
            this.txtBxStockPrice.Size = new System.Drawing.Size(122, 20);
            this.txtBxStockPrice.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(101, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Stock Price:";
            // 
            // txtBxUnitPrice
            // 
            this.txtBxUnitPrice.Location = new System.Drawing.Point(222, 78);
            this.txtBxUnitPrice.Name = "txtBxUnitPrice";
            this.txtBxUnitPrice.Size = new System.Drawing.Size(122, 20);
            this.txtBxUnitPrice.TabIndex = 33;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(101, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Unit Price:";
            // 
            // txtBxName
            // 
            this.txtBxName.Location = new System.Drawing.Point(222, 38);
            this.txtBxName.Name = "txtBxName";
            this.txtBxName.Size = new System.Drawing.Size(122, 20);
            this.txtBxName.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(101, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Name:";
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(222, 196);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(122, 111);
            this.pictureBox.TabIndex = 27;
            this.pictureBox.TabStop = false;
            // 
            // NewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 400);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtBxStockPrice);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBxUnitPrice);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBxName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.btnSaveProduct);
            this.Controls.Add(this.btnGetPicture);
            this.Name = "NewForm";
            this.Text = "New Product";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSaveProduct;
        private System.Windows.Forms.Button btnGetPicture;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBxStockPrice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBxUnitPrice;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBxName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox;
    }
}