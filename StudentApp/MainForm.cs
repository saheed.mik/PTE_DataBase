﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentApp
{
    public partial class formMain : Form
    {
        StudentDbModelContainer studentContainer;
        public formMain()
        {
            InitializeComponent();
            studentContainer = new StudentDbModelContainer();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewStudentForm newStudentForm = new NewStudentForm(studentContainer);
            newStudentForm.ShowDialog(this);
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteStudentForm deleteStudentForm = new DeleteStudentForm(studentContainer);
            deleteStudentForm.ShowDialog(this);
        }

        private void countToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("We currently have " + studentContainer.Students.Count() +
                                       " Students stored in the Database.", "Information");
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditStudentForm editStudentForm = new EditStudentForm(studentContainer);
            editStudentForm.ShowDialog(this);
        }
    }
}
