﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FruitStore
{
    public partial class PurchaseForm : Form
    {
        private ProductModel dataModel;
        private Product selectedProduct = new Product();
        Random random = new Random();
        private double currentWeight;

        public PurchaseForm()
        {
            InitializeComponent();
        }

        public PurchaseForm(ProductModel dataModel):this()
        {
            btnWeigh.Click += BtnWeigh_Click;
            btnPay.Click += BtnPay_Click;
            this.dataModel = dataModel;

            flowLayoutPanel.WrapContents = true;
            flowLayoutPanel.AutoScroll = true;
            string images_folder = @"images";

            try
            {
                foreach (var item in dataModel.Products)
                {
                    Button button = new Button
                    {
                        BackgroundImage = Image.FromFile(Path.Combine(images_folder, item.Picture)),
                        Tag = item.ProductID
                    };

                    button.Click += ProductButton_Click;
                    flowLayoutPanel.Controls.Add(button);
                }
            }
            catch (IOException)
            {

                MessageBox.Show("Error accessing Image  file");
            }
        }

        private void BtnPay_Click(object sender, EventArgs e)
        {
            
            var entity = dataModel.Products.Find(selectedProduct.ProductID);
            if (entity != null)
            {
                selectedProduct.StockValue -= currentWeight;
                entity.StockValue -= currentWeight;
                //dataModel.Entry(entity).CurrentValues.SetValues(selectedProduct);
                dataModel.SaveChanges();
            }

            txtBxAmount.Clear();
            txtBxDetails.Clear();
            txtBxWeight.Clear();
            MessageBox.Show("THANK YOU");
        }

        private void BtnWeigh_Click(object sender, EventArgs e)
        {

            if (selectedProduct.StockValue > 0)
            {
                lblSelectProduct.Text = selectedProduct.ProductName;
                currentWeight = Math.Round(random.NextDouble() * (selectedProduct.StockValue + 1.0),2);
                double total = currentWeight * selectedProduct.UnitPrice;
                txtBxWeight.Text = currentWeight.ToString();
                txtBxAmount.Text = total.ToString();

                txtBxDetails.Text = currentWeight.ToString() + " (kg) " + selectedProduct.ProductName
                                    + " * " + selectedProduct.UnitPrice.ToString();
            }
            else
            {
                MessageBox.Show("Sorry " + selectedProduct.ProductName + " is out of stock");
            }
        }

        public Product SelectedProduct
        {
            get => selectedProduct;
            set
            {
                selectedProduct = value;
            }
        }

        private void ProductButton_Click(object sender, EventArgs e)
        {
            btnWeigh.Enabled = true;

            Button button = (Button)sender;
            selectedProduct = dataModel.Products.Find(button.Tag) ?? new Product();

            lblSelectProduct.Text = selectedProduct.ProductName;            

        }
    }
}
