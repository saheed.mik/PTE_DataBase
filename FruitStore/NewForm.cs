﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FruitStore
{
    public partial class NewForm : Form
    {
        private ProductModel dataModel;
        Product product;
        Image image;
        String imageName;

        public NewForm()
        {
            InitializeComponent();
        }

        public NewForm(ProductModel dataModel):this()
        {
            this.dataModel = dataModel;
            product = new Product();
        }

        private void btnGetPicture_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                image = Image.FromFile(openFileDialog.FileName);
                imageName = openFileDialog.SafeFileName;
                pictureBox.Image = image;

                //using (var ms = new MemoryStream())
                //{
                //    image.Save(ms, image.RawFormat);
                //    product.Picture = ms.ToArray().ToString();
                //}
                
            }
        }

        private void btnSaveProduct_Click(object sender, EventArgs e)
        {
            try
            {
                product.ProductName = txtBxName.Text;
                product.StockValue = Double.Parse(txtBxStockPrice.Text);
                product.UnitPrice = Double.Parse(txtBxUnitPrice.Text);

                image.Save(@"images\" + imageName);
                product.Picture = imageName;

                dataModel.Products.Add(product);
                dataModel.SaveChanges();
            }
            catch (IOException)
            {
                MessageBox.Show("Error trying to copy product image.\nMake sure you have Write access to disk");

            }
        }
    }
}
